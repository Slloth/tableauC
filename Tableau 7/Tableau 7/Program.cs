﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
enum Week {Lundi, Mardi, Mercredi, Jeudi, Vendredi, Samedi, Dimanche};

namespace Tableau_7
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string i in Enum.GetNames(typeof(Week))) // Permet d'afficher les constantes de l'énumération
            {
                Console.WriteLine(i);
            }
        
            Console.WriteLine("\n"+Enum.GetName(typeof(Week),4)); // Affiche la Valeur possèdant l'indice n°4
        }
    }
}
