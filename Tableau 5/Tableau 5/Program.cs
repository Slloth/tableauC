﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tableau_5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> Numbers = new List<int>(); // création de la liste
            Numbers.Add(2);
            Numbers.Add(3);
            Numbers.Add(4);
            Numbers.Add(5);
            Numbers.Add(6);
            foreach (int Number in Numbers) // Boucle PourChaque
            {
                Console.WriteLine(Number);
            }                               //Boucle de sortie
            Console.WriteLine("\n");
            Numbers.Insert(0,1);            //La fonction Insert permet d'insert une Valeur à l'indice voulut
            Numbers.Insert(6,7);
            foreach (int Number in Numbers)
            {
                Console.WriteLine(Number);
            }
        }
    }
}
