﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tableau_8
{
    enum Week { Lundi=1, Mardi, Mercredi, Jeudi, Vendredi, Samedi, Dimanche };  //LA PLUS GROSSE MAGOUILLE DU MONDE !!!!!
    class Program
    {
        static void Main(string[] args)
        {
            foreach (string i in Enum.GetNames(typeof(Week))) // Permet d'afficher les constantes de l'énumération
            {
                Console.WriteLine(i);
            }
            Console.WriteLine("\n" + Enum.GetName(typeof(Week), 4)); // Permet d'afficher la constantes N°4
        }
    }
}
